# README #

A demo project of using Selenium and Python for automation retrieval text resources

### What is this repository for? ###

Summary: This is a POC project of using Selenium to walkthrough the WFO site to extract the text resources and compare the dataset of different languages vs pseudo language for hardcode/non-localize data.



#### Getting it working ####

- Install python (use anaconda if you don't alerady have something)
- Create a virtualenvironment to install the needed libraries into
    - conda create --name selenium
    - activate snowflakes
    - pip install -r requires.txt
- run it:
    python go.py

- look over the output: out.html
