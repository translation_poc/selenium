"""
what is pageHiddenAriaLiveRegion?
what are omse other we conventinos we can leverlage off of?

do we need to check the document.title?
what about titles?

** we can probably use the check to see if  aria-label or label is aria-labelledby in addition to label

div.vdropdown
    is a fake dropdown?
        has vname and vonchange

"""


import atexit
import codecs
from collections import namedtuple
from functools import partial
import time

from jinja2 import Environment, FileSystemLoader, select_autoescape

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


env = Environment(
    loader=FileSystemLoader('./templates'),
    autoescape=select_autoescape(['html', 'xml'])
)



def init_driver_chrome():
    """ Initialize a ChromeDriver using an IE user agent to get around the websites's error.

        We also set an implicit timeout of 10seconds to find things
    """
    opts = Options()
    opts.add_argument("user-agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'")

    driver = webdriver.Chrome(chrome_options=opts)
    driver.implicitly_wait(10)

    # driver.xpath = lambda x: get_xpath_from_element(driver, x)
    driver.xpath = partial(get_xpath_from_element, driver)

    # close the driver when we are done
    @atexit.register
    def finalizer():
        driver.close()

    return driver

def init_driver_ff():
    """ Initialize a FireFox using an IE user agent to get around the websites's error.

        We also set an implicit timeout of 10seconds to find things
    """
    profile = webdriver.FirefoxProfile()
    profile.set_preference("general.useragent.override", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36")
    driver = webdriver.Firefox(profile)


    driver.implicitly_wait(10)

    # driver.xpath = lambda x: get_xpath_from_element(driver, x)
    driver.xpath = partial(get_xpath_from_element, driver)

    # close the driver when we are done
    @atexit.register
    def finalizer():
        driver.close()

    return driver


def init_driver_phantomjs():
    """ Initialize a ChromeDriver using an IE user agent to get around the websites's error.

page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36';
page.viewportSize = { width: 1680, height: 1050 };

        We also set an implicit timeout of 10seconds to find things
    """
    #opts = Options()
    #opts.add_argument("user-agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'")

    driver = webdriver.PhantomJS('phantomjs')
    #driver = webdriver.Chrome(chrome_options=opts)
    driver.implicitly_wait(10)

    # driver.xpath = lambda x: get_xpath_from_element(driver, x)
    driver.xpath = partial(get_xpath_from_element, driver)

    # close the driver when we are done
    @atexit.register
    def finalizer():
        driver.close()

    return driver


init_driver = init_driver_chrome




def convert_to_test_language(url='http://10.156.14.59/wfo/control/signin', driver=None):
    """

    """
    driver = driver if driver is not None else init_driver()

    driver.get(url)

    # ToDo: Do this in a more black-box nature
    driver.execute_script('$("input[name=language]").val("te_ST");')
    driver.execute_script('handleLanguageChange($("[vname=Language]")[0]);')

    return driver


def login(url=None, username='wsuperuser', password='pumpkin1', driver=None):

    driver = driver if driver is not None else init_driver()
    if url:
        driver.get(url)

    assert "Verint" in  driver.title

    username_field = driver.find_element_by_id("username")
    username_field.send_keys(username)

    password_field = driver.find_element_by_id("password")
    password_field.send_keys(password)

    go = driver.find_element_by_id('loginToolbar_LOGINLabel')
    go.click()

    return driver

def get_xpath_from_element(driver, element):
    """ get a form of xpath for this element.

        ToDo: Have it return role= if the document has it...
    """
    return driver.execute_script("gPt=function(c){if(c.id!==''){return'#'+c.id}if(c===document.body){return c.tagName}var a=0;var e=c.parentNode.childNodes;for(var b=0;b<e.length;b++){var d=e[b];if(d===c){return gPt(c.parentNode)+'/'+c.tagName.toLowerCase()+'['+(a+1)+']'}if(d.nodeType===1&&d.tagName===c.tagName){a++}}};return gPt(arguments[0]);", element)



YieldReturn = namedtuple('YieldReturn', 'path text classes role'.split())
SKIP_TAGS = set("script img".split())
def depth_first(element, driver):
    """ yield the interesting elements in depth first order

        Intersesting is definded as:

            - has text
            - has element has a title or aria-label or label is aria-labelledby
    """
    #print "1", time.time()
    if "skip-translate" in element.get_attribute("class"):
        return

    if element.tag_name in SKIP_TAGS:
        return

    #print "element:", element.tag_name
    #children = element.find_elements_by_xpath("./*")
    children = element.find_elements_by_css_selector("*")
    #print "2", time.time(),len(children)

    if len(children) != 0:
        for child in children:
            for e in depth_first(child, driver):
                #if e.text:
                #    yield YieldReturn(driver.xpath(e), e.text, e.get_attribute("class"), e.get_attribute("role"))
                yield e

    #print "3", time.time()
    if element.text:
        yield YieldReturn(driver.xpath(element), element.text, element.get_attribute("class"), element.get_attribute("role"))

# driver = login(url='http://10.156.14.59/wfo/control/signin')

driver = convert_to_test_language()
time.sleep(5)
body = driver.find_element_by_css_selector("body")


# http://stackoverflow.com/questions/2214952/jquery-hasclass-check-for-more-than-one-class

script = """
// get node text
$.fn.myText = function() {
    var str = '';

    this.contents().each(function() {
        if (this.nodeType == 3) {
            str += this.textContent || this.innerText || '';
        }
    });

    return str;
};

// minimized function to get get a form of xpath for the element
gPt=function(c){if(c.id!==''){return'#'+c.id}if(c===document.body){return c.tagName}var a=0;var e=c.parentNode.childNodes;for(var b=0;b<e.length;b++){var d=e[b];if(d===c){return gPt(c.parentNode)+'/'+c.tagName.toLowerCase()+'['+(a+1)+']'}if(d.nodeType===1&&d.tagName===c.tagName){a++}}};

var nodes = [];

/*
    Work way up tree to find nearest role.
*/
function findNearestRole(e){
    var parents = e.parents("[role]");

    var ret = [];

    parents.each(function(i,e){
        ret.push($(e).attr("role"));
    });

    return ret;
}

function addNode(path, nodeName, classList, text, role, nearestRole){
    var r = {};

    r.path = path
    r.nodeName = nodeName;
    r.classList = classList;
    r.text = text;
    r.role = role;
    r.nearestRole = nearestRole;

    nodes.push(r);
}

function recurse(e){
    e = $(e);
    if (e.filter(".skip-translate").size() != 0) {
        console.log("filter class " + e[0].classList);
        return;
    }

    if (e.filter("script, style").size() != 0){
        console.log("filter tag " + e[0].nodeName);
        return;
    }

    // add the element
    addNode(gPt(e[0]), e[0].nodeName, e[0].classList, e.myText().trim(), e.attr("role"), findNearestRole(e));

    // now check if any attributes need to be added
    $.each(["aria-label", "value"], function(index, attr){
        var attr_value = e.attr(attr)
        if (attr_value != null) {
            addNode(gPt(e[0])+"["+attr+"]", e[0].nodeName+"["+attr+"]", null, attr_value.trim(), null, findNearestRole(e));
        }
    });

    e.children().each(function(index, element) {
        recurse(element);
    });
}


recurse($("body").get(0));
return nodes;
"""

nodes = driver.execute_script(script);

# quickly filter out the empty rows
nodes = [x for x in nodes if x.get('text')]

template = env.get_template("page.html")

s = template.render(nodes=nodes)
with codecs.open("out.html", "wt","utf-8") as f:
    f.write(s)
#
#
#
# for node in nodes:
#     if node['text']:
#         print u"{path} -> {nodeName} : [{text}] {{{classList}}} role={role}, nearestRole={nearestRole}".format(**node)
#





